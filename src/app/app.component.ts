import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  loadedFeature = 'recipe';

  ngOnInit() {
    firebase.initializeApp({
      apiKey: 'AIzaSyB6vEEQsLq4uRCPFAHSa_EPQV67D2PUhpQ',
      authDomain: 'udemy-ng-http-xxxx.firebaseapp.com',
      databaseURL: 'https://udemy-ng-http-xxxx.firebaseio.com',
    });
  }

  onNavigate(feature: string) {
    this.loadedFeature = feature;
  }
}
