import * as AuthActions from './auth.actions';
import { AuthTypes } from './auth.actions';

export interface State {
  token: string;
  authenticated: boolean;
}

const initialState: State = {
  token: null,
  authenticated: false
}

export function authReducer(state = initialState, action: AuthActions.AuthActions) {
  switch (action.type) {
    case AuthTypes.SIGNUP:
    case AuthTypes.SIGNIN:
      return {
        ...state,
        authenticated: true,
      };
    case AuthTypes.SET_TOKEN:
      return {
        ...state,
        token: action.payload
      };
    case AuthTypes.LOGOUT:
      return {
        ...state,
        authenticated: false,
        token: null
      };
    default: return state;
  }
}
