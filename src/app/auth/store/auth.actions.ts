import { Action } from '@ngrx/store';

export enum AuthTypes {
  TRY_SIGNIN = '[Auth] Try sign in',
  TRY_SIGNUP = '[Auth] Try sign up',
  SIGNUP = '[Auth] Sign up',
  SIGNIN = '[Auth] Sign in',
  LOGOUT = '[Auth] Log out',
  SET_TOKEN = '[Auth] Set token'
}

export class TrySignUp implements Action {
  readonly type = AuthTypes.TRY_SIGNUP;

  constructor(public payload: { username: string, password: string }) {
  }
}

export class TrySignIn implements Action {
  readonly type = AuthTypes.TRY_SIGNIN;

  constructor(public payload: { username: string, password: string }) {
  }
}

export class Signup implements Action {
  readonly type = AuthTypes.SIGNUP;
}

export class Signin implements Action {
  readonly type = AuthTypes.SIGNIN;
}

export class Logout implements Action {
  readonly type = AuthTypes.LOGOUT;
}

export class SetToken implements Action {
  readonly type = AuthTypes.SET_TOKEN;

  constructor(public payload: string) {
  }
}

export type AuthActions =
  Signup |
  Signin |
  Logout |
  SetToken |
  TrySignUp |
  TrySignIn;
