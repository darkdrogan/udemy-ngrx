import { Component, OnInit } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { ActivatedRoute, Params, Router } from '@angular/router';

import * as ShoppingListActions from '../../shopping-list/store/shopping-list.actions';
import { Observable } from 'rxjs';
import { FeatureState, State } from '../store/recipe.reducers';
import { take } from 'rxjs/operators';
import { DeleteRecipe } from '../store/recipe.actions';

@Component({
  selector: 'app-recipe-detail',
  templateUrl: './recipe-detail.component.html',
  styleUrls: ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {
  recipesState$: Observable<State>;
  id: number;

  constructor(private route: ActivatedRoute,
              private router: Router,
              private store: Store<FeatureState>) {
  }

  ngOnInit() {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.recipesState$ = this.store.pipe(
            select('recipesState')
          );
        }
      );
  }

  onAddToShoppingList() {
    this.store.pipe(
      take(1),
      select('recipesState')
    )
      .subscribe(({recipes}) => {
        this.store.dispatch(new ShoppingListActions.AddIngredients(recipes[this.id].ingredients));
      });
  }

  onEditRecipe() {
    this.router.navigate(['edit'], {relativeTo: this.route});
  }

  onDeleteRecipe() {
    this.store.dispatch(new DeleteRecipe(this.id));
    this.router.navigate(['/recipes']);
  }

}
