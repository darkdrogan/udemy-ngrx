import { Ingredient } from '../../shared/ingredient.model';
import { Recipe } from '../recipe.model';
import { RecipeActions, RecipeTypes } from './recipe.actions';
import { AppState } from '../../store/app.reducers';

export interface FeatureState extends AppState {
  recipesState: State;
}

export interface State {
  recipes: Recipe[];
}

const initialState: State = {
  recipes: [
    new Recipe(
      'Tasty Schnitzel',
      'A super-tasty Schnitzel - just awesome!',
      'https://upload.wikimedia.org/wikipedia/commons/7/72/Schnitzel.JPG',
      [
        new Ingredient('Meat', 1),
        new Ingredient('French Fries', 20)
      ]),
    new Recipe('Big Fat Burger',
      'What else you need to say?',
      'https://upload.wikimedia.org/wikipedia/commons/b/be/Burger_King_Angus_Bacon_%26_Cheese_Steak_Burger.jpg',
      [
        new Ingredient('Buns', 2),
        new Ingredient('Meat', 1)
      ])
  ],
};

export function recipeReducer(state = initialState, action: RecipeActions) {
  switch (action.type) {
    case RecipeTypes.ADD_RECIPE:
      return {
        ...state,
        recipes: [
          ...state.recipes,
          action.payload
        ]
      };
    case RecipeTypes.DELETE_RECIPE:
      const recipes = state.recipes.filter((value, index) => index !== action.payload);
      // alternative way
      // const oldRecipes = [...state.recipes];
      // oldRecipes.splice(action.payload, 1);
      return {
        ...state,
        recipes
      };
    case RecipeTypes.SET_RECIPES:
      return {
        ...state,
        recipes: [...action.payload]
      };
    case RecipeTypes.UPDATE_RECIPE:
      const updatedRecipe = state.recipes.map((value, index) => {
        if (index === action.payload.index) {
          return action.payload.updatedRecipe;
        }
        return value;
      });
      return {
        ...state,
        recipes: [...updatedRecipe]
      };

    default:
      return state;
  }
}
