import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { map, mergeMap, switchMap, take, tap } from 'rxjs/operators';
import { Router } from '@angular/router';
import { fromPromise } from 'rxjs/internal-compatibility';

import * as AuthActions from './auth.actions';
import * as firebase from 'firebase';

@Injectable()
export class AuthEffects {

  @Effect()
  authSignup = this.actions$
    .pipe(
      ofType(AuthActions.AuthTypes.TRY_SIGNUP),
      map((action: AuthActions.TrySignUp) => action.payload),
      switchMap((authData: { username: string, password: string }) => {
        return fromPromise(firebase.auth().createUserWithEmailAndPassword(authData.username, authData.password));
      }),
      switchMap(() => {
        return fromPromise(firebase.auth().currentUser.getIdToken());
      }),
      mergeMap((token: string) => {
        this.router.navigate(['/']);
        return [
          {
            type: AuthActions.AuthTypes.SIGNUP
          },
          {
            type: AuthActions.AuthTypes.SET_TOKEN,
            payload: token
          }
        ];
      })
    );

  @Effect()
  authSignin = this.actions$.pipe(
    ofType(AuthActions.AuthTypes.TRY_SIGNIN),
    map((action: AuthActions.TrySignIn) => action.payload),
    switchMap(({username, password}) => {
      return fromPromise(firebase.auth().signInWithEmailAndPassword(username, password));
    }),
    switchMap(() => fromPromise(firebase.auth().currentUser.getIdToken())),
    mergeMap((token: string) => {
      this.router.navigate(['/']);
      return [
        {
          type: AuthActions.AuthTypes.SIGNIN
        },
        {
          type: AuthActions.AuthTypes.SET_TOKEN,
          payload: token
        }
      ];
    })
  );

  @Effect({dispatch: false})
  authLogout = this.actions$.pipe(
    ofType(AuthActions.AuthTypes.LOGOUT),
    tap( () => this.router.navigate(['/']))
  );

  constructor(private actions$: Actions, private router: Router) {
  }
}
