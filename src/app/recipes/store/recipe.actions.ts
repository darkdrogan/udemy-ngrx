import { Action } from '@ngrx/store';
import { Recipe } from '../recipe.model';

export enum RecipeTypes {
  ADD_RECIPE = '[Recipe] Add recipe',
  DELETE_RECIPE = '[Recipe] Delete recipe',
  STORE_RECIPES = '[Recipe] Store recipes',
  STORED_RECIPES = '[Recipe Stored recipes',
  FETCH_RECIPES = '[Recipe] Fetch recipes',
  SET_RECIPES = '[Recipe] Set recipe',
  UPDATE_RECIPE = '[Recipe] Update recipe',
}

export class AddRecipe implements Action {
  readonly type = RecipeTypes.ADD_RECIPE;

  constructor(public payload: Recipe) {
  }
}

export class DeleteRecipe implements Action {
  readonly type = RecipeTypes.DELETE_RECIPE;

  constructor(public payload: number) {
  }
}

export class FetchRecipes implements Action {
  readonly type = RecipeTypes.FETCH_RECIPES;
}

export class StoreRecipes implements Action {
  readonly type = RecipeTypes.STORE_RECIPES;
}

export class SetRecipes implements Action {
  readonly type = RecipeTypes.SET_RECIPES;

  constructor(public payload: Recipe[]) {
  }
}

export class UpdateRecipe implements Action {
  readonly type = RecipeTypes.UPDATE_RECIPE;

  constructor(public payload: { index: number, updatedRecipe: Recipe }) {
  }
}

export type RecipeActions =
  AddRecipe |
  DeleteRecipe |
  StoreRecipes |
  FetchRecipes |
  SetRecipes |
  UpdateRecipe;
