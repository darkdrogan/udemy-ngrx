import { Actions, Effect, ofType } from '@ngrx/effects';
import { HttpClient, HttpRequest } from '@angular/common/http';
import { map, switchMap, take, withLatestFrom } from 'rxjs/operators';

import { Recipe } from '../recipe.model';
import { FetchRecipes, RecipeTypes, SetRecipes, StoreRecipes } from './recipe.actions';
import { Injectable } from '@angular/core';
import { FeatureState } from './recipe.reducers';
import { select, Store } from '@ngrx/store';

@Injectable()
export class RecipesEffect {
  @Effect()
  recipeFetch = this.actions$.pipe(
    ofType(RecipeTypes.FETCH_RECIPES),
    switchMap((action: FetchRecipes) => {
      return this.httpClient.get<Recipe[]>('https://udemy-ng-http-xxxx.firebaseio.com/recipes.json', {
        observe: 'body',
        responseType: 'json'
      });
    }),
    map(
      (recipes) => {
        console.log(recipes);
        for (const recipe of recipes) {
          if (!recipe['ingredients']) {
            recipe['ingredients'] = [];
          }
        }
        return {type: RecipeTypes.SET_RECIPES, payload: recipes};
      }
    )
  );

  @Effect({dispatch: false})
  recipeStore = this.actions$.pipe(
    ofType(RecipeTypes.STORE_RECIPES),
    withLatestFrom(this.store.pipe(select('recipesState'))),
    switchMap(([action, state]) => {
      console.log(state);
      const req = new HttpRequest('PUT', 'https://udemy-ng-http-xxxx.firebaseio.com/recipes.json',
        state.recipes, {reportProgress: true});
      return this.httpClient.request(req);
    })
    // альтернативный путь
    // map((action: StoreRecipes) => {
    //   this.store.pipe(
    //     select('recipesState'),
    //     take(1)
    //   ).subscribe(
    //     (({recipes}) => {
    //       console.log(recipes);
    //       this.httpClient.put('https://udemy-ng-http-xxxx.firebaseio.com/recipes.json', recipes).subscribe(
    //         () => console.log('super')
    //       );
    //     })
    //   );
    // })
  );

  constructor(private actions$: Actions,
              private httpClient: HttpClient,
              private store: Store<FeatureState>) {
  }
}
